"use strict";

const { signIn, signUp } = require("../users");
const User = require("../../models/users");
const service = require("../../services/index");
describe("Users", () => {
  const userMock = {
    _id: 1,
    name: 123,
    picture: 123,
    category: "computers",
    price: 123,
    description: 123,
  };

  it("signIn", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };
    jest.spyOn(User, "find").mockImplementation((id, cb) => cb(null, userMock));
    jest.spyOn(service, "createToken").mockImplementation(() => 1);
    const req = {
      body: { email: "example@email.com" },
    };
    await signIn(req, mockResponse);
    expect(mockResponse.send).toHaveBeenCalledTimes(1);
    expect(mockResponse.send).toHaveBeenCalledWith({ token: 1 });

    expect(mockResponse.status).toHaveBeenCalledTimes(1);
    expect(mockResponse.status).toHaveBeenCalledWith(200);
  });
  it("[No User]signIn", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };
    jest.spyOn(User, "find").mockImplementation((id, cb) => cb(null, null));

    const req = {
      body: { email: "example@email.com" },
    };
    await signIn(req, mockResponse);
    expect(mockResponse.send).toHaveBeenCalledTimes(1);
    expect(mockResponse.send).toHaveBeenCalledWith({
      message: "no existe el usuario",
    });

    expect(mockResponse.status).toHaveBeenCalledTimes(1);
    expect(mockResponse.status).toHaveBeenCalledWith(404);
  });
  it("[ERROR] signIn", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };
    jest.spyOn(User, "find").mockImplementation((id, cb) => cb("error", null));
    const req = {
      body: { email: "example@email.com" },
    };
    await signIn(req, mockResponse);
    expect(mockResponse.send).toHaveBeenCalledTimes(1);
    expect(mockResponse.send).toHaveBeenCalledWith({ message: "error" });

    expect(mockResponse.status).toHaveBeenCalledTimes(1);
    expect(mockResponse.status).toHaveBeenCalledWith(500);
  });
  it("signUp", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };
    jest
      .spyOn(User.prototype, "save")
      .mockImplementation((cb) => cb(null, { ...userMock, id: 1 }));
    jest.spyOn(service, "createToken").mockImplementation(() => 1);
    const req = {
      body: { email: "example@email.com" },
    };
    await signUp(req, mockResponse);
    expect(mockResponse.send).toHaveBeenCalledTimes(1);
    expect(mockResponse.send).toHaveBeenCalledWith({ token: 1 });

    expect(mockResponse.status).toHaveBeenCalledTimes(1);
    expect(mockResponse.status).toHaveBeenCalledWith(200);
  });
  it("[ERROR] signUp", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };
    jest
      .spyOn(User.prototype, "save")
      .mockImplementation((cb) => cb("error", { ...userMock, id: 1 }));
    const req = {
      body: { email: "example@email.com" },
    };
    await signUp(req, mockResponse);
    expect(mockResponse.send).toHaveBeenCalledTimes(1);
    expect(mockResponse.send).toHaveBeenCalledWith({
      message: "error al crear usuario",
    });

    expect(mockResponse.status).toHaveBeenCalledTimes(1);
    expect(mockResponse.status).toHaveBeenCalledWith(500);
  });
});
